package test;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;

/**
 * Created by ROM_PC on 04.07.2016.
 */
public class Main {

    private static ChromeDriverService service;
    private static WebDriver driver;

    public static void main(String[] args) throws Exception {
        createAndStartService();
        createDriver();
        testGoogleSearch();
        quitDriver();
        createAndStopService();
    }

    public static final int MILLIS = 1500;

    //http://chromedriver.storage.googleapis.com/index.html?path=2.9/
    //https://seleniumhq.github.io/selenium/docs/api/java/org/openqa/selenium/chrome/ChromeDriver.html
    //https://github.com/junit-team/junit4/wiki/Download-and-Install
    //http://docs.seleniumhq.org/download/maven.jsp
    //java -cp MySeleniumTest.jar test.Main

    public static void createAndStartService() throws Exception {
        service = new ChromeDriverService.Builder()
                .usingDriverExecutable(new File("chromedriver_win32/chromedriver.exe"))
                .usingAnyFreePort()
                .build();
        service.start();
    }


    public static void createAndStopService() {
        service.stop();
    }


    public static void createDriver() {
        driver = new RemoteWebDriver(service.getUrl(),
                DesiredCapabilities.chrome());
    }


    public static void quitDriver() {
        driver.quit();
    }

    public static void testGoogleSearch() throws InterruptedException {
        driver.get("https://www.instagram.com/");
        Thread.sleep(MILLIS);
        WebElement vhod = driver.findElement(By.className("_k6cv7"));
        vhod.click();
        Thread.sleep(MILLIS);

        WebElement username = driver.findElement(By.name("username"));
        username.sendKeys("username");
        Thread.sleep(MILLIS);

        WebElement pass = driver.findElement(By.name("password"));
        pass.sendKeys("pass");
        Thread.sleep(MILLIS);

        WebElement button = driver.findElement(new By.ByTagName("button"));
        button.submit();
        Thread.sleep(MILLIS);
        Thread.sleep(MILLIS);
        Thread.sleep(MILLIS);

        WebElement like = driver.findElement(By.linkText("Нравится"));
        like.click();
        Thread.sleep(MILLIS);

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,400)");
        Thread.sleep(MILLIS);

        WebElement like1 = driver.findElement(By.linkText("Нравится"));
        like1.click();
        Thread.sleep(MILLIS);

        JavascriptExecutor js1 = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,400)");
        Thread.sleep(MILLIS);

        WebElement like2 = driver.findElement(By.linkText("Нравится"));
        like2.click();
        Thread.sleep(MILLIS);

        js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,400)");
        Thread.sleep(MILLIS);

        js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,250)");
        Thread.sleep(MILLIS);
        js.executeScript("window.scrollBy(0,250)");
        Thread.sleep(MILLIS);
        //assertEquals("webdriver - Поиск в Google", driver.getTitle());
        driver.quit();

    }
}
